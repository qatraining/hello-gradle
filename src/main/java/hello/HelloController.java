package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Properties;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "<h1>Greetings from Spring Boot!</h1>\n" +
        "<p>You can see the best type of dinosaur by going to /dinosaur</p>"; //+ "\n" + appInsights;
    }

    @RequestMapping("/dinosaur")
    public String dino() {
      return "The best dinosaur is a Triceratops!";// + "\n" + appInsights;
    }

    @RequestMapping("/properties")
    public String properties() throws IOException {
      InputStream is = new FileInputStream("config/application.properties");
      Properties props = new Properties();
      props.load(is);
      is.close();
      return "The name key in the properties file is: " + props.getProperty("name");// + "\n" + appInsights;
    }

    // String appInsights = "<script type=\"text/javascript\">" +
    //     "var appInsights=window.appInsights||function(config){" +
    //         "function r(config){t[config]=function(){var i=arguments;t.queue.push(function(){t[config].apply(t,i)})}}var t={config:config},u=document,e=window,o=\"script\",s=u.createElement(o),i,f;s.src=config.url||\"//az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(s);try{t.cookie=u.cookie}catch(h){}for(t.queue=[],i=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];i.length;)r(\"track\"+i.pop());return r(\"setAuthenticatedUserContext\"),r(\"clearAuthenticatedUserContext\"),config.disableExceptionTracking||(i=\"onerror\",r(\"_\"+i),f=e[i],e[i]=function(config,r,u,e,o){var s=f&&f(config,r,u,e,o);return s!==!0&&t[\"_\"+i](config,r,u,e,o),s}),t" +
    //     "}({" +
    //         "instrumentationKey:\"INSERT-KEY-HERE\"" +
    //     "});" +
    //     "window.appInsights=appInsights;" +
    //     "appInsights.trackPageView();" +
    // "</script>";
}
